<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<fmt:setLocale value="${pageContext.request.locale.language}" />
<c:choose>
	<c:when test="${pageContext.request.locale.language == 'fr'}">
		<fmt:setBundle basename="com.exercises.config.app_fr" var="lang"
			scope="session" />
	</c:when>
	<c:when test="${pageContext.request.locale.language == 'en'}">
		<fmt:setBundle basename="com.exercises.config.app" var="lang"
			scope="session" />
	</c:when>
</c:choose>
<html>

<head>
<title><fmt:message key="loginTitle" bundle="${lang}"></fmt:message></title>
<link rel="stylesheet" href="styles/reset.css">
<link rel="stylesheet" href="styles/login-styles.css">
</head>

<body>

	<%@include file="header.jsp"%>

	<jsp:useBean id="user" class="com.exercises.bean.User" />

	<section>

		<section class="register-card">

			<form method="post" action="login">

				<div class="name-input">
					<div class="text-input">
						<input tabindex="3" name="name" class="input-text" type="text"
							required>
						<jsp:setProperty property="name" name="user" />
						<span class="highlight"></span> <span class="bar"></span> <label
							class="input-label"> <fmt:message key="username"
								bundle="${lang}"></fmt:message>
						</label>
					</div>
				</div>

				<div class="name-input">
					<div class="text-input">
						<input tabindex="3" name="pass" class="input-text" type="password"
							required>
						<jsp:setProperty property="pass" name="user" />
						<span class="highlight"></span> <span class="bar"></span> <label
							class="input-label"> <fmt:message key="password"
								bundle="${lang}"></fmt:message>
						</label>
					</div>
				</div>

				<div id="login-button" class="login-button">
					<button type="submit" tabindex="6" class="login ripple">
						<fmt:message key="login" bundle="${lang}"></fmt:message>
					</button>
				</div>

			</form>

		</section>

	</section>
</body>

</html>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<fmt:setLocale value="${pageContext.request.locale.language}" />
<c:choose>
	<c:when test="${pageContext.request.locale.language == 'fr'}">
		<fmt:setBundle basename="com.exercises.config.app_fr" var="lang"
			scope="session" />
	</c:when>
	<c:when test="${pageContext.request.locale.language == 'en'}">
		<fmt:setBundle basename="com.exercises.config.app" var="lang"
			scope="session" />
	</c:when>
</c:choose>


<html>

<head>
<title><fmt:message key="indexTitle" bundle="${lang}"></fmt:message></title>
<link rel="stylesheet" href="styles/reset.css">
<link rel="stylesheet" href="styles/styles.css">
</head>

<body>

	<%@include file="header.jsp"%>

	<jsp:useBean id="user" class="com.exercises.bean.User" />


	<section>

		<section class="register-card">

			<form method="post" action="register">
				<div id="tab1">

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="name" class="input-text" type="text"
								required>
							<jsp:setProperty property="name" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="username"
									bundle="${lang}"></fmt:message></label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="pass" class="input-text"
								type="password" required>
							<jsp:setProperty property="pass" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="password"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>
				</div>

				<div id="tab2">

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="plot_no" class="input-text" type="text"
								required>
							<jsp:setProperty property="plot_no" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="plotNumber"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="door_no" class="input-text" type="text"
								required>
							<jsp:setProperty property="door_no" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="doorNumber"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="street" class="input-text" type="text"
								required>
							<jsp:setProperty property="street" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="street"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="area" class="input-text" type="text"
								required>
							<jsp:setProperty property="area" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="area"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="country" class="input-text" type="text"
								required>
							<jsp:setProperty property="country" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="country"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

					<div class="name-input">
						<div class="text-input">
							<input tabindex="3" name="pincode" class="input-text" type="text"
								required>
							<jsp:setProperty property="pincode" name="user" />
							<span class="highlight"></span> <span class="bar"></span> <label
								class="input-label"> <fmt:message key="pinCode"
									bundle="${lang}"></fmt:message>
							</label>
						</div>
					</div>

				</div>

				<div id="login-button" class="login-button">
					<button onclick="nextTab()" tabindex="6" class="login ripple">
						<fmt:message key="next" bundle="${lang}"></fmt:message>
					</button>
				</div>

				<div class="register-button" id="submit-button">
					<button onclick="previous()" tabindex="6" class="login ripple">
						<fmt:message key="previous" bundle="${lang}"></fmt:message>
					</button>
					<button type="submit" tabindex="6" class="login ripple">
						<fmt:message key="submit" bundle="${lang}"></fmt:message>
					</button>
				</div>

				<a href="login.jsp"> <fmt:message key="haveAcc" bundle="${lang}"></fmt:message>
				</a>
			</form>

		</section>
	</section>

	</fmt:bundle>

	<script src="scripts/index-scripts.js"></script>

</body>

</html>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<fmt:setLocale value="${pageContext.request.locale.language}" />
<c:choose>
	<c:when test="${pageContext.request.locale.language == 'fr'}">
		<fmt:setBundle basename="com.exercises.config.app_fr" var="lang"
			scope="session" />
	</c:when>
	<c:when test="${pageContext.request.locale.language == 'en'}">
		<fmt:setBundle basename="com.exercises.config.app" var="lang"
			scope="session" />
	</c:when>
</c:choose>
<html>

<head>
<title><fmt:message key="homeTitle"></fmt:message></title>
<link rel="stylesheet" href="styles/reset.css">
<link rel="stylesheet" href="styles/home-styles.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
</head>

<body>

	<fmt:bundle basename="com.exercises.config.app">

		<%@include file="header.jsp"%>

		<jsp:useBean id="user" class="com.exercises.bean.User" />

		<%@ page language="java" contentType="text/html"%>
		<%@ page import="java.util.*"%>
		<%@ page import="com.exercises.bean.User"%>
		<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>

		<section class="container">

			<div class="table-card">
				<div class="student-table">

					<table id="table">

						<thead>
							<tr>
								<th><fmt:message key="plotNoTable"></fmt:message></th>
								<th><fmt:message key="doorNoTable"></fmt:message></th>
								<th><fmt:message key="streetTable"></fmt:message></th>
								<th><fmt:message key="areaTable"></fmt:message></th>
								<th><fmt:message key="countryTable"></fmt:message></th>
								<th><fmt:message key="pinCodeTable"></fmt:message></th>
							</tr>
						</thead>

						<tbody>
							<c:forEach items="${requestScope.values}" var="value">
								<tr>
									<td><c:out value="${value.getPlot_no()}" /></td>
									<td><c:out value="${value.getDoor_no()}" /></td>
									<td><c:out value="${value.getArea()}" /></td>
									<td><c:out value="${value.getStreet()}" /></td>
									<td><c:out value="${value.getCountry()}" /></td>
									<td><c:out value="${value.getPincode()}" /></td>
								</tr>
							</c:forEach>
						</tbody>

					</table>
				</div>
			</div>

			<div id="form" class="form-card">
				<form method="post" action="addAddress">
					<div class="input-sec">
						<input type="number" name="plot_no"
							placeholder=<fmt:message key="plotNoTable"></fmt:message>>
						<jsp:setProperty property="plot_no" name="user" />
						<input type="text" name="door_no"
							placeholder=<fmt:message key="doorNoTable"></fmt:message>>
						<jsp:setProperty property="door_no" name="user" />
						<input type="text" name="street"
							placeholder=<fmt:message key="streetTable"></fmt:message>>
						<jsp:setProperty property="street" name="user" />
						<input type="text" name="area"
							placeholder=<fmt:message key="areaTable"></fmt:message>>
						<jsp:setProperty property="area" name="user" />
						<input type="text" name="country"
							placeholder=<fmt:message key="countryTable"></fmt:message>>
						<jsp:setProperty property="country" name="user" />
						<input type="text" name="pincode"
							placeholder=<fmt:message key="pinCodeTable"></fmt:message>>
						<jsp:setProperty property="pincode" name="user" />
					</div>
					<div class="submit-sec">
						<!-- <input type="button" name="button" Value="Submit" onClick="submit(this.form)"> -->
						<input type="submit" class="ripple" name="button" Value="SUBMIT"
							onClick="testResults(this.form)">
					</div>
				</form>
			</div>
		</section>

		<div class="fab">
			<button onclick="fab()" class="ripple">
				<i class="material-icons"><fmt:message key="descriptionIcon"></fmt:message></i>
			</button>
		</div>

	</fmt:bundle>

	<script src="scripts/home-scripts.js"></script>
</body>

</html>
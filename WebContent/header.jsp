<link rel="stylesheet" href="styles/header-styles.css">
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html"%>
<%@ page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt"%>
<nav class="nav-bar">
	<fmt:bundle basename="com.exercises.config.app">
		<c:choose>
			<c:when test="${requestScope.name != null}">
				<h1>
					<fmt:message key="headerMessage"></fmt:message>
					<c:out value="${requestScope.name}" />
				</h1>
			</c:when>
			<c:otherwise>
				<h1>
					<fmt:message key="headerTitle"></fmt:message>
				</h1>
			</c:otherwise>
		</c:choose>
	</fmt:bundle>
</nav>
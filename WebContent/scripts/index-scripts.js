document.getElementById("tab2").style.display = "none"
document.getElementById("submit-button").style.display = "none"

function nextTab() {
	document.getElementById("tab2").style.display = "flex"
	document.getElementById("submit-button").style.display = "flex"

	document.getElementById("tab1").style.display = "none"
	document.getElementById("login-button").style.display = "none"
}

function previous() {
	document.getElementById("tab2").style.display = "none"
	document.getElementById("submit-button").style.display = "none"

	document.getElementById("tab1").style.display = "flex"
	document.getElementById("login-button").style.display = "flex"
}
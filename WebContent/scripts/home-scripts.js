function testResults(form) {
    var plotNumber = form.plotNumber.value;
    var doorNumber = form.doorNumber.value;
    var street = form.street.value;
    var area = form.area.value;
    var country = form.country.value;
    var pincode = form.pincode.value;
    if (plotNumber && doorNumber && street && area && country &&
        pincode) {
        var table = document.getElementById("table");
        var len = document.getElementById("table").rows.length;
        var row = table.insertRow(len);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        var cell6 = row.insertCell(5);
        cell1.innerHTML = plotNumber;
        cell2.innerHTML = doorNumber;
        cell3.innerHTML = street;
        cell4.innerHTML = area;
        cell5.innerHTML = country;
        cell6.innerHTML = pincode;
        reset(form);
    } else {
        alert("Please, Fill up all the fields :|")
    }
}

function fab() {
    var element = document.getElementById("form");
    if (element.style.display == "flex") {
        element.style.display = "none";
    } else {
        element.style.display = "flex";
    }
}

function reset(form) {
    form.plotNumber.value = "";
    form.doorNumber.value = "";
    form.street.value = "";
    form.area.value = "";
    form.country.value = "";
    form.pincode.value = "";
}
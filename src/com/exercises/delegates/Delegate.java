package com.exercises.delegates;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import com.exercises.bean.Address;
import com.exercises.bean.User;
import com.exercises.queries.Query;

public class Delegate {

	public static User registerUser(HttpServletRequest req) {
		User user = new User();
		user.setName(req.getParameter("name"));
		user.setPass(req.getParameter("pass"));
		return user;
	}
	
	public static Address registerAddress(HttpServletRequest req) {
		Address address = new Address();
		address.setPlot_no(req.getParameter("plot_no"));
		address.setDoor_no(req.getParameter("door_no"));
		address.setStreet(req.getParameter("street"));
		address.setArea(req.getParameter("area"));
		address.setCountry(req.getParameter("country"));
		address.setPincode(req.getParameter("pincode"));
		return address;
	}
	
	public static User loginUser(HttpServletRequest req) {
		User user = new User();
		user.setName(req.getParameter("name"));
		user.setPass(req.getParameter("pass"));
		return user;
	}
	
	public static Address addAddress(HttpServletRequest req) {
		Address address = new Address();
		address.setPlot_no(req.getParameter("plot_no"));
		address.setDoor_no(req.getParameter("door_no"));
		address.setStreet(req.getParameter("street"));
		address.setArea(req.getParameter("area"));
		address.setCountry(req.getParameter("country"));
		address.setPincode(req.getParameter("pincode"));
		return address;
	}
	
	public static int register(Connection con, User user) throws SQLException {
		PreparedStatement ps = con.prepareStatement(Query.REGISTER_USERS);
		ps.setString(1, user.getName());
		ps.setString(2, user.getPass());
		int status = ps.executeUpdate();
		return status;
	}
	
	public static int address(Connection con, Address address, String userId) throws SQLException {
		PreparedStatement ps = con.prepareStatement(Query.REGISTER_ADDRESS);
		ps.setString(1, userId);
		ps.setString(2, address.getPlot_no());
		ps.setString(3, address.getDoor_no());
		ps.setString(4, address.getStreet());
		ps.setString(5, address.getArea());
		ps.setString(6, address.getCountry());
		ps.setString(7, address.getPincode());
		int status = ps.executeUpdate();
		return status;
	}
	
}

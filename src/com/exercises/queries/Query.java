package com.exercises.queries;

public class Query {
	
	public static final String REGISTER_USERS = "insert into users(username, pass) values(?,?)";
	
	public static final String USER_ID = "select userid from users where username=?";
	
	public static final String REGISTER_ADDRESS = "insert into address(userid, plot_no, door_no, street, area, country, pincode) values(?,?,?,?,?,?,?)";
	
	public static final String LOGIN_USER = "select * from users where username=? and pass=?";
	
	public static final String FETCH_USERS = "select * from users where username=?";
	
	public static final String FETCH_ADDRESS = "select * from address where userid=?";
	
	public static final String FETCH_USER_ID = "select userid from users where username=?";
	
}

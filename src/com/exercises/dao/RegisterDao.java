package com.exercises.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.exercises.bean.Address;
import com.exercises.bean.User;
import com.exercises.db.DBConnection;
import com.exercises.queries.Query;

public class RegisterDao {

	public static int register(User user, Address address) {
		int status = 0;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(Query.REGISTER_USERS);
			System.out.println(user.getName());
			System.out.println(user.getPass());
			ps.setString(1, user.getName());
			ps.setString(2, user.getPass());
			status = ps.executeUpdate();
			if(status > 0) {
				ps = con.prepareStatement(Query.USER_ID);
				ps.setString(1, user.getName());
				ResultSet resultSet = ps.executeQuery();
				while(resultSet.next()) {
					String userid = resultSet.getString("userid");
					user.setUserid(userid);
				}
				ps = con
						.prepareStatement(Query.REGISTER_ADDRESS);
				ps.setString(1, user.getUserid());
				ps.setString(2, address.getPlot_no());
				ps.setString(3, address.getDoor_no());
				ps.setString(4, address.getStreet());
				ps.setString(5, address.getArea());
				ps.setString(6, address.getCountry());
				ps.setString(7, address.getPincode());
				status = ps.executeUpdate();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return status;
	}
	
	public static boolean login(User user) {
		boolean statement = false;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(Query.LOGIN_USER);
			System.out.println(user.getName());
			System.out.println(user.getPass());
			ps.setString(1, user.getName());
			ps.setString(2, user.getPass());
			ResultSet resultSet = ps.executeQuery();
			statement = resultSet.next();
			
		}catch(Exception e) {
			e.printStackTrace();
		}
		return statement;
	}
	
	public static List getValues(String name) {
		User user = new User();
		List<Address> addresses = new ArrayList<>();
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(Query.FETCH_USERS);
			ps.setString(1, name);
			ResultSet resultSet = ps.executeQuery();
			int userid = 0;
			while(resultSet.next()) {
				userid = resultSet.getInt("userid");
			}
			if(userid != 0) {
				ps = con.prepareStatement(Query.FETCH_ADDRESS);
				ps.setInt(1, userid);
				resultSet = ps.executeQuery();
				while(resultSet.next()) {
					Address address = new Address();
					address.setPlot_no(resultSet.getString("plot_no"));
					address.setDoor_no(resultSet.getString("door_no"));
					address.setStreet(resultSet.getString("street"));
					address.setArea(resultSet.getString("area"));
					address.setCountry(resultSet.getString("country"));
					address.setPincode(resultSet.getString("pincode"));
					addresses.add(address);
				}
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return addresses;
	}
	
	public static int addAddress(String username, Address address) {
		int status = 0;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(Query.FETCH_USER_ID);
			ps.setString(1, username);
			ResultSet resultSet = ps.executeQuery();
			int userid = 0;
			while(resultSet.next()) {
				userid = resultSet.getInt("userid");
			}
			if(userid != 0) {
				ps = con.prepareStatement(Query.REGISTER_ADDRESS);
				ps.setInt(1, userid);
				ps.setString(2, address.getPlot_no());
				ps.setString(3, address.getDoor_no());
				ps.setString(4, address.getStreet());
				ps.setString(5, address.getArea());
				ps.setString(6, address.getCountry());
				ps.setString(7, address.getPincode());
				status = ps.executeUpdate();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return status;
	}

}

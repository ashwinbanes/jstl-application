package com.exercises.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;

import com.exercises.bean.User;
import com.exercises.dao.RegisterDao;
import com.exercises.delegates.Delegate;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		User user = Delegate.loginUser(req);
		String name = user.getName();

		boolean isLoggedIn = RegisterDao.login(user);

		if (isLoggedIn) {
			RequestDispatcher rd = req.getRequestDispatcher("home.jsp");
			HttpSession session = req.getSession();
			session.setAttribute("name", name);
			req.setAttribute("name", name);
			List<User> values = RegisterDao.getValues(name);
			req.setAttribute("values", values);
			rd.forward(req, resp);
		}
	}
}

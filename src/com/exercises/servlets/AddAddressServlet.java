package com.exercises.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.exercises.bean.Address;
import com.exercises.bean.User;
import com.exercises.dao.RegisterDao;
import com.exercises.delegates.Delegate;

@WebServlet("/addAddress")
public class AddAddressServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Address address = Delegate.addAddress(req);
		HttpSession session = req.getSession();
		String name = (String) session.getAttribute("name");
		int status = RegisterDao.addAddress(name, address);
		req.setAttribute("name", name);
		if(status > 0) {
			RequestDispatcher rd = req.getRequestDispatcher("home.jsp");
			List<Address> addresses = RegisterDao.getValues(name);
			req.setAttribute("values", addresses);
			rd.forward(req, resp);
		}else {
			System.out.println("Somethings been screwed up :(");
		}
	}
}

package com.exercises.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.exercises.bean.Address;
import com.exercises.bean.User;
import com.exercises.dao.RegisterDao;
import com.exercises.delegates.Delegate;

@WebServlet("/register")
public class RegisterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		resp.setContentType("text/html;charset=UTF-8");
		PrintWriter out = resp.getWriter();
		
		User user = Delegate.registerUser(req);
		Address address = Delegate.registerAddress(req);

		int status = RegisterDao.register(user, address);

		if (status > 0) {
			RequestDispatcher rd = req.getRequestDispatcher("login.jsp");
			rd.forward(req, resp);
		} else {
			out.println("<script>Try after some time :(</script>");
		}

	}

}
